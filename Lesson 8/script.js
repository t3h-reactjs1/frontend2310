//C1
const listNumber = [1, 2, 3, true, 'name', null, undefined];

//C2
const listNumber2 = new Array(1,2,3);

//===BT1===
console.log('===BT1===');
// let a = new Array('a','b','c');
// let b = new Array('a','b','c');
let a = ['a','b','c'];
let b = ['a','b','c'];
console.log(typeof a);
console.log(typeof b);
console.log(a+b);
console.log(a==b);


