

=======================
Lesson 3:
https://docs.google.com/presentation/d/1ud6rYIZUTR1se8eeaHDE9r47GkIysSFHLzqI4-qsDic/edit?pli=1#slide=id.g153a7a106e6_0_27


=======================
Lesson 4:



=======================
Lesson 10:
https://docs.google.com/presentation/d/1Fd5wBGmn0SU8dUG_aNaBzQkJlAELVFFSmdcfw4aa4_0/edit#slide=id.g15c79668462_0_103


=======================
Lesson 11:

https://github.com/DungPHPandREACT/tech-stack-mern/blob/master/L%C3%BD%20thuy%E1%BA%BFt/Module3-ECMA-features-%26-Typescript/Lesson1/Syllabus.md

https://viblo.asia/p/tim-hieu-co-ban-ve-es6-DKBedxBZedX

https://hocjavascript.net/tong-quan/tong-quan-ve-javascript-es6/

BT thực hành:
https://github.com/DungPHPandREACT/tech-stack-mern/blob/master/B%C3%A0i%20t%E1%BA%ADp%20th%E1%BB%B1c%20h%C3%A0nh/Javascript/Es6(2).md
1. 2
2. 1
3. 1
4. 3
5. 4
6. 
7. 3
8. 2
9. 3
10. 2
11. 
12. 




https://github.com/DungPHPandREACT/tech-stack-mern/blob/master/B%C3%A0i%20t%E1%BA%ADp%20th%E1%BB%B1c%20h%C3%A0nh/Javascript/ES6.txt

 ** Arrow fuction:
    - ko có hoisting
    - ko có context
        + ko có  binding cho this, super. 
        (this của arrow function là context gần nhất với nó, thay vì là đối tượng gọi nó.)
    - Không thể dùng như một “object constructor”.

=================
Lesson 12
Ghi dữ liệu vào local storage
	localStorage.setItem(key, value)
	=> Khi mà muốn lưu trữ object hay array -> convert dữ liệu
	đó sang định dạng json
	JSON.stringify(value)
Lấy dữ liệu từ local storage
	localStorage.getItem(key)
Xóa dữ liệu ở local storage
	localStorage.removeItem(key);
Xóa toàn bộ dữ liệu ở local storage
	localStorage.clear()
