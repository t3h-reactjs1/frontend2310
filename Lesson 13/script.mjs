import { renderStudents } from "./feature.mjs";

const dataStudents = [
    {
        id: 1,
        name: 'Trần Văn A',
        gender: 'nam',
        math_score: 10,
        english_score: 10,
        literature_score: 10,
        average_score: 10,
    },
    {
        id: 2,
        name: 'Nguyễn Văn B',
        gender: 'nam',
        math_score: 9,
        english_score: 9,
        literature_score: 9,
        average_score: 9,
    }
]



renderStudents(dataStudents);

export {
    dataStudents
}

