import {dataStudents} from './script.mjs'

const renderStudents = (listStudent) =>{
    let rowsHTML = '';
    for (let student of listStudent){
        rowsHTML += `
            <tr>
                <th scope="row">${student.id}</th>
                <td>${student.name}</td>
                <td>${student.gender}</td>
                <td>${student.math_score}</td>
                <td>${student.english_score}</td>
                <td>${student.literature_score}</td>
                <td>${student.average_score}</td>
            </tr>
        `
    }
    document.getElementById('show-students').innerHTML = rowsHTML;
    console.log(rowsHTML);

}

const createStusdent = () =>{
    const id = document.getElementById('id').value;
    const name = document.getElementById('name').value;
    const gender = document.getElementById('gender').value;
    const math_score = document.getElementById('math_score').value;
    const english_score = document.getElementById('english_score').value;
    const literature_score = document.getElementById('literature_score').value;

    const newstudent = {
        id,
        name,
        gender,
        math_score,
        english_score,
        literature_score,
    };
    
    dataStudents.push(newstudent);

    renderStudents.apply(dataStudents);

}

export {
    renderStudents
};
