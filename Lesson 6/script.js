

const greeting = 'Hello world!';
//alert (greeting);
//BT1
let x, y, z;
x = true; y = true; z = false;
console.log(x && y && z); //false
console.log(x && y && !z); //true
console.log((x && y) || z); //true
console.log((x && y) || !z); //true
console.log(x && (y || z)); //true
console.log(x && !(y || z)); //false
console.log(x && (y || !z)); //true
console.log(x && (!y || z)); //false

//BT2
let name = 'AAA', age = 20, isGood = true, age2 = "10";

console.log(age + age2); //2010
console.log(age + 10); //30
console.log(age % 3); //2
console.log(name + ' BBB'); //AAA BBB
console.log(!isGood); //false
console.log(name == 'aaa' && age >= 20); //false
console.log(name != 'aaa' && isGood); //true
console.log(!(age < 10) && !isGood); //false
console.log(12 + 3 + 4 + '5'); //195
console.log(12 + 3 + 4 + '5' + 6); //'1956' // phép cộng: số bị ép thành chuỗi
// Phép [-, *, /]: chuỗi bị ép thành số
console.log(12 + 3 + 4 + '5' - 6); //189 
console.log(12 + 3 + 4 + '5' * 1); //19 + (5) = 24 
console.log(12 + 3 + 4 + '14' / 2); // 19 + (7) = 26

console.log('========BT3============');
//BT3
let a = 10, b = 20, c = 30, d = '40';
console.log(a + b + c); // 60
console.log(a - b / c) ; //9.333
console.log(a - (b * c)); //10 - 600 = -590
console.log(d - (a * b) - c); // 40 - 200 - 30 = -190
console.log(a + b + c + d);// 10 + 20 + 30 + '40' = 60 + '40' = 6040
console.log(d + a + b + c);// '40' + 10 + 20 + 30 = "4010" + 20 + 30 = "401020" + 30 = "40102030"
console.log(d + a - b + c);// '40' + 10 - 20 + 30 = '4010' - 20 + 30 = 3990 + 30 = 4020
console.log(a - b + d - c);// 10 - 20 + '40' - 30 = -10 + '40' - 30 = '-1040' - 30 = -1070
console.log(d - c + a - b);// '40' - 30 + 10 - 20 = 10 + 10 - 20 = 0
console.log(a * b + d * c);// 10 * 20 + '40' * 30 = 200 + '40' * 30 = 200 + 1200 = 1400

//
console.log('========BT4============');
x = true;   y = false; z = 10;

console.log (x && y); 
console.log( x && !y);
console.log('======');
console.log (x && z == 10);
console.log ((x && z == 10));
console.log (!x && z == 10);
console.log (!x && z == 50);
console.log('======');
console.log (x && y && z == 10); //false
console.log( x && !y && z == 10);//true
console.log (x || y || z == 10);//true
console.log( (x && y) || z != 10);//false
console.log (!(x && z == 10) || y);//fale
//
console.log('========BT5============');
a = 1, b = '2', c = 3, d = '4';

console.log (b + d); //24
console.log (a + b + c + d); // '12' + 3 + '4' = 1234
console.log (a - b + c - d); // -1 + 3 - '4' = -2
console.log (a - b - c + d ); // -1 - 3 + '4' = "-44"
console.log ((b + d) - (a + c)); // ("24") - (4) = 20
console.log ((a + b) - (c + d)); // "12" - "34" = -22
console.log (a * c + b * d );// 3 + '2' * '4' = 3 + 8 = 11
console.log (-b + d); // -'2' + '4' = "-24"
console.log (-b - d); // -'2' - '4' = -6
console.log (-(b + d)); // -("2" + "4") = -("24") = "-24"
//
console.log('========BT6============');
age = 25;
let isMarried = false, isRich = true;

console.log(age > 25 && isMarried && isRich) ; //false
console.log((age <= 25 || isMarried) && isRich) ; // true
console.log((age > 10 || isRich) && isMarried) ; // false
console.log(!(age >= 15 && isMarried) && isRich) ;// true
console.log('======');
console.log(!(age <= 20) || !(isMarried && isRich) );//true
console.log((age > 8 && !isMarried) || isRich) ;//true
console.log(!(age < 8 && !isMarried) || isRich) ;// true
console.log((age == 8 && isMarried) || !isRich) ;// false

console.log('========BT7============');
a = 5, b = 10, c = 15;

console.log(a + b);// 15
console.log(a - c);// -10
console.log(a + b + c);// 30
console.log(a + c + b);//30
console.log((a + b) % 3);// 0
console.log('======');

console.log(a * b > 50) ;// false
console.log(a ** b); //9765625
console.log(a ** b > 100) ;//true
console.log(c - (b++) == 5) ;// 15 - 10 = 5 == 5 >> true
console.log(b); // b= 11
console.log(c - (++b) == 5) ; // 15 - (12) = 3 == 5 >> false

console.log('========BT8============');
let isTall = true, isMuscle = false, isHandsome = true;

console.log(isTall && isMuscle) ;//false
console.log(!isHandsome) ;//false
console.log(!isTall || !isMuscle );//true
console.log(isTall || !(isMuscle && isHandsome)) ;//true
console.log(isMuscle && !(isTall || isHandsome)) ;//false

//
console.log('========BT9============');
a = 10, b = 5, c = '1';

console.log(a + b); //15
console.log(a - c); //9
console.log(a + b + c); //'151'
console.log(a + c + b); //'101' + 5 = '1015'
console.log((a + b) % 3); //15 % 3 = 0
console.log('======');
console.log(a * b > 50); //false
console.log(a ** b ); //100000
console.log(a ** b > 100); //true
console.log(a - (b++) == 5); //5 => true
//b = 6
console.log(a - (++b) == 5); //3 => false

//
console.log('========BT10============');
isTall = true, isMuscle = false, isHandsome = true;

console.log(isTall && isMuscle); //false
console.log(!isHandsome); //false
console.log(!isTall || !isMuscle);//true
console.log(isTall || !(isMuscle && isHandsome));//true
console.log(isMuscle && !(isTall || isHandsome));//false
