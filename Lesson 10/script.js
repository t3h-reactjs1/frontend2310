
let $myTag1 = document.getElementById('h1')
let $myTag2 = document.getElementsByClassName('h1')
let $myTag3 = document.getElementsByTagName('h1')
let $tagA = document.getElementById('tagA')

console.log($myTag2);

console.log($myTag3);

//Thay đổi noi dung

$myTag1.innerHTML = '<h1>Hello!!!</h1>';
console.log($myTag1.innerText);
console.log($myTag1.innerHTML);

console.log($tagA.getAttribute('href'));

$tagA.setAttribute('href', 'https://www.youtube.com/');
$tagA.setAttribute('target', '_blank');

console.log($tagA.getAttribute('href'));
console.log($tagA.getAttribute('target'));

//

$myTag1.style.color = 'red';
$myTag1.style.fontSize = '30px';
$myTag1.style.display = 'block';
//

//const text = prompt('Mời bạn nhập nội dung');
const  text = 'Test';
const $newTagLi = document.createElement('li');
$newTagLi.innerText = text;
document.getElementById('favorite-list').appendChild($newTagLi);

const $tafli = document.getElementsByClassName('favorite-list-item');
for(let i = 0; i < $tafli.length; i++){
    $tafli[i].innerHTML = 'I love this thing';
    $tafli[i].style.color = 'red';
}

//event
function notify(){
    alert('Xin chào thế giới!');
}
function notifyHello(){
    alert('Hello!');
}
//c1
//document.getElementById('btn-click').onclick = notify;
//c2
// document.getElementById('btn-click').onclick = function(){
//     alert('Xin chào thế giới!');
// }

// document.getElementById('btn-click').addEventListener('click', function(){
//     alert('Hello!');
// })
//c3
document.getElementById('btn-click').addEventListener('click', notify);
document.getElementById('btn-click').addEventListener('click', notifyHello);

