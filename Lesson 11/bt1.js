//bt1
/**
 * Bài 1: Viết 1 hàm tính tổng các số trong dãy
    TH1: 1,2,3,4
    TH2: 1,2,3,4,5,6
    TH3: 1,2,3,4,5,6,7,8
    TH4: 1,2,4

    function sum () {
        ...
    }

    sum(1,2,3,4) => 
    sum(1,2,3,4,5,6) => 
    sum(1,2,3,4,5,6,7,8) => 
    sum(1,2,4)
 */

//rest operator: tham số truyền vào là 1 array, luôn phải để cuối cùng
const sum = (...lstNumber) => { 
    //pread operator : ..., Rest operator là cách để tạo ra một function với vô hạn tham số truyền vào.
    let sum = 0;
    for(let number of lstNumber){
        sum += number;
    }
    return sum;
}

//console.log(sum(1,2,3,4)); //a = 1, b = 2, sum = 7
//console.log(sum(2,3));

//console.log(sum(1,2,3,4,5,6));

// ** pread operator : ..., Rest operator là cách để tạo ra một function với vô hạn tham số truyền vào.
const logger = (a,b,c, ...lstNumber) => { 
   
    console.log(a);
    console.log(b);
    console.log(c);
    console.log(lstNumber);
}

// logger(1,2);
// logger(1,2,3);
// logger(1,2,3,4,5);

//bt2
// const student1 = { name: "Trần Văn A", age: 18 }
// const student2 = student1;
// student2.name = "Nguyễn Văn B";
// // console.log(student1)
// // console.log(student2)

//bt3
const student1 = { name: "Trần Văn A", age: 18 }
//spread operator
const student2 = {...student1, gender: 'boy'};
const student3 = {...student1, gender: 'boy', name:'Oanh Vu'}; //name sẽ bị đè = name:'Oanh Vu' 
    //Object:
    //key name là unique, value sẽ bị đè = name:'Oanh Vu' 
    //code chạy từ trái qua phải
const student4 = {gender: 'boy', name:'Oanh Vu', ...student1}; //name: "Trần Văn A"
student2.name = "Nguyễn Văn B";
console.log(student1)
console.log(student2)
console.log(student3)
console.log(student4)

//bt4: copy arr vào vị trí bất kỳ và thêm các phần tử bất kỳ vào 1 mảng

const arr1 = [1,2,3];
const arr2 = [...arr1, 4,5,6];
let arr3 = [4,5, ...arr1,6];
//arr2[0] = 20;
console.log(arr1)
console.log(arr2)
console.log(arr3)

//bt5: can duplicate spread operator
const arr4 = [1,2,3];
const arr5 = [4,5, ...arr1, 6, ...arr1, ...arr1, ...arr1];
//arr2[0] = 20;
console.log(arr5)