//BT1 - Đảo chuỗi
const reverseString = (string) => {
    // let result = '';
    // for(let i = string.length-1; i > 0; i--){
    //     console.log(string[i]);
    // }
    // return result;
    return [...string].reverse().join('');
}

//c1//
//reverseString('123456');

console.log(`BT1: ${ reverseString('123456')}`);

/* ============================== */
//BT2
/* dùng map(): tăng mỗi phần tử của mảng truyền vào lên 5 
Array.map(callback) // callback nhận vào 3 tham số
 - value: trả về ptu đang dc duyet
 - index:
 - 
=> kết quả của map là trã về 1 mảng mới
*/
const arr=[1,2,3,4];
const newArr = arr.map((value, index) => value + 5);

console.log(`BT2: `); 
console.log(newArr);

/* ============================== */
//BT3: tạo ra newArr có các ptu là số lẻ: dùng hàm filter
const listNumber = [1,3,5,7,8,10];
const newListNumber = listNumber.filter((number) => { //lọc các ptu thòa đk bên dưới: number % 2 ===1
    return number % 2 ===1;
})
console.log(`BT3: `); 
console.log(newListNumber);

